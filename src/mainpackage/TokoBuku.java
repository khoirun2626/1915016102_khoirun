package mainpackage;
import java.util.ArrayList;
import java.util.Scanner;
import tokobuku.sejarah1;

public class TokoBuku {
    
    public static void main(String[] args) {
    int halaman,harga,jumlahBuku,indeks;
    String nama,pencipta;
    Scanner input = new Scanner (System.in);
    ArrayList<sejarah1> ListSejarah1 = new ArrayList<>();
    
    OUTER :
        while (true) {
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            System.out.println("|\t DATA                   |");
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            System.out.println("1. Tambah Buku ");
            System.out.println("2. Tampil Buku ");
            System.out.println("3. Hapus Buku ");
            System.out.println("4. Cari Buku ");
            System.out.println("5. Exit ");
            System.out.println("\n Masukkan Pilihan : ");
            int pilihan = input.nextInt();
            System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            switch(pilihan){
                case 1 :
                    sejarah1 atributbuku = new sejarah1();
                    System.out.println("Judul buku      : ");
                    nama = input.next();
                    System.out.println("Nama Pencipta   : ");
                    pencipta = input.next();
                    System.out.println("Kode Buku       : ");
                    jumlahBuku = input.nextInt();
                    System.out.println("Jumlah Halaman  : ");
                    halaman = input.nextInt();
                    System.out.println("Harga Buku      : ");
                    harga = input.nextInt();

                    atributbuku.setNamaBuku(nama);
                    atributbuku.setPencipta(pencipta);
                    atributbuku.setJumlah(jumlahBuku);
                    atributbuku.setHalaman(halaman);
                    atributbuku.setHarga(harga);

                    ListSejarah1.add(atributbuku);
                    break;
                case 2 :
                    jumlahBuku = ListSejarah1.size();
                    for (int i=0;i<jumlahBuku;i++){
                        System.out.println("data buku ke"+(1+1));
                        System.out.println("judul buku      : "+ListSejarah1.get(i).getNamaBuku());
                        System.out.println("nama pencipta   : "+ListSejarah1.get(i).getPencipta());
                        System.out.println("jumlah buku     : "+ListSejarah1.get(i).getJumlah());
                        System.out.println("jumlah Halaman  : "+ListSejarah1.get(i).getHalaman());
                        System.out.println("Harga Buku      : "+ListSejarah1.get(i).getHarga());
                    }break;
                case 3 :
                    try {
                        System.out.println("Masukkan Index yang ingin diubah : ");
                        indeks = input.nextInt();
                        ListSejarah1.remove(indeks);
                    }
                    catch(IndexOutOfBoundsException u){
                        System.out.println("indeks tidak ditemukan ");
                    }
                    break;
                case 4 :
                    System.out.println("Masukkan kode buku yang di cari : ");
                    int indek = input.nextInt();
                    for (int i=0;i<ListSejarah1.size();i++){
                        if(indek == ListSejarah1.get(i).getJumlah()){
                            System.out.println("judul buku      : "+i+ " : "+ListSejarah1.get(i).getNamaBuku());
                            System.out.println("nama pencipta   : "+i+ " : "+ListSejarah1.get(i).getPencipta());
                            System.out.println("jumlah halaman  : "+i+ " : "+ListSejarah1.get(i).getHalaman());
                            System.out.println("Harga Buku      : "+i+ " : "+ListSejarah1.get(i).getHarga());
                        }
                    break OUTER;
                    }
                case 5 :
                    System.out.println("Terima kasih");
                    break OUTER;
                default :
                    break;
                
            }
        }
    }
}